<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sdn_description' => 'Une série de noisettes spécifiques à SPIPr-Dane',
	'sdn_nom' => 'SPIPr-Dane Noisettes',
	'sdn_slogan' => 'Des noisettes pour SPIPr-Dane.',
);
