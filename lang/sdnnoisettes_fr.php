<?php
/**
  SPIPr-Dane-Noisettes
  Fichier noisettes_fr.php
  (c) 2019 Dominique Lepaisant
  Distribue sous licence GPL3
*/
$GLOBALS[$GLOBALS['idx_lang']] = array(

/* noisettes */
'afficher'=>'Afficher',
'afficher_articles_secteur'=>'Afficher les articles du secteur',
'afficher_articles_selection'=>'N\'afficher que les articles sélectionnés',
'afficher_articles_rubriques_selection'=>'N\'afficher que les articles des rubriques sélectionnées',
'afficher_documents_tous'=>'Afficher tous les documents',
'afficher_docs_selection'=>'N\'afficher que les documents sélectionnés',
'afficher_docs_article'=>'Afficher les documents d\'un article',
'afficher_docs_rubrique'=>'Afficher les documents d\'une rubrique',
'alert_activer_breves'=>'Attention, vous devez <a href="./?exec=configurer_contenu#activer_breves_oui">activer l\'utilisation des brèves</a> pour utiliser cette noisette.',
'alert_activer_sites'=>'Attention, vous devez <a href="./?exec=configurer_contenu#activer_sites_oui">activer la gestion\'annuaire de sites web</a> pour utiliser cette noisette.',
'article_contenu'=>'Contenu de l\'article',
'article_documents'=>'Documents de l\'article',
'article_forum'=>'Forum de l\'article',
'article_evenements'=>'&Eacute;vénements de l\'article',
'article_portfolio'=>'Galerie d\'images de l\'article',
'article_ecrire_auteur'=>'&Eacute;crire à l\'auteur',
    
'nom_carousel'=>'Carousel',
'nom_article_a_la_une'=>'Article à la une',
'carte_gis'=>'Carte',
 
'data_localisation' => 'Localisation',
'date' => 'Date',
'description_article_contenu' => 'Affiche le titre, le texte de l\'article, et s\'ils existent, le logo et les autres éléments activés <i>(sur-titre, sous-titre, chapo, etc...)</i>.',
'description_article_documents' => 'Affiche les documents liès à l\'article en cours.',
'description_calendrier_mini' => 'Affiche un mini-calendrier mensuel navigable avec un lien pointant vers une page au choix en filtrant les résultats sur le jour en question.',
'description_carousel'=>'Bannière animée d\'images et de textes.',
'description_liste_articles'=>'Liste d\'articles d\'une rubrique particulère ou non.',
'description_liste_articles_meme_rubrique'=>'Liste des articles dans la même rubrique.',
'description_liste_articles_selection'=>'Liste des articles séléctionnés à la racine du site pour la page sommaire, des articles sélectionnés de la rubrique pour une page rubrique. ',
'description_liste_evenements'=>'Liste des événements à venir. Nécessite le plugin Agenda.',
'description_liste_mots'=>'Liste de mots-clé d\'un ou de plusieurs groupes de mots.',
'description_liste_rubriques_soeurs'=>'Liste des rubriques de même parent.',
'description_liste_sites'=>'Liste des sites référencés.',
'description_sites_partenaires'=>'Liste des sites partenaires.',
'description_article_ecrire_auteur'=>'Formulaire de contact de l\'auteur.',
'description_article_a_la_une'=>'Un article sélectionné',

'info_galerie'=>'Galerie',

'erreur_uai' => 'Attention&#44; vous devez <a href="@url@"> saisir le code UAI de votre établissement</a>.',
'explication_afficher_filtre'=>'Affiche le titre du mot-clé filtrant les articles.',
'explication_articles_choisis'=>'Cliquer sur "Ajouter" à droite et sélectionner les articles à inclure ou exclure de la liste.',
'explication_article_choisi'=>'Cliquer sur «&nbsp;Ajouter&nbsp;» à droite et sélectionner l\'article.',
'explication_article_a_la_une'=>'Permet de choisir un article à afficher entièrement (suivant les champs paramétrés).',
#'explication_articles_exclus'=>'Cliquer sur "Ajouter" à droite et sélectionner les articles à exclure de la liste.',
'explication_carousel'=>'Les articles affichés dans le carousel sont les articles séléctionnés à la racine du site pour l\'affichage<br/> dans la page d\'accueil et ceux selectionnés dans la rubrique pour l\'affichage dans une page rubrique.',
'explication_carousel_id_rubrique'=>'Par défaut, affiche les articles sélectionnés à la racine du site. <br/>Vous pouvez choisir d\'afficher les articles sélectionnés d\'une rubrique particuliére.',
'explication_choix_coordonnees'=>'Si la position fournie (latitude et longitude) est imprécise, vous pouvez la modifier.',
'explication_conf_avancee_noisettes'=>'<strong>/!\ Attention, vous devez avoir une connaisance minimum du html5 pour utiliser cette option./!\</strong>',
'explication_docs_choisis'=>'Saisissez les identifiants (<i>numéros</i>) des documents à afficher dans la liste, séparés par une virgule.',
'explication_docs_articles_choisis'=>'Cliquer sur "Ajouter" à droite et sélectionner l\'article dont les documents sont à afficher.',
'explication_docs_rubriques_choisies'=>'Cliquer sur "Ajouter" à droite et sélectionner la rubriquee dont les documents sont à afficher.',
'explication_est_controleur'=>'Utilise le motif d\'architecture <a href="https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur" title="Wikipedia : Modéle-Vue-Côntroleur">MVC (<i>Modèle-Vue-Contrôleur</i>)</a> pour contrôler une noisette de la même page.<br/>Au clic sur un lien, le bloc controlé (vue) sera rechargé avec les valeurs des paramètres passés par le lien.',
'explication_est_vue'=>'Sélectionner la noisette (vue) qui sera visée par le contôleur. <br/>Cette noisette doit être présente dans la même page que le contrôleur, <br/>mais pas nécessairement dans le même bloc.',
'explication_exclure_article_selection'=>'Vous pouvez exclure les articles sélectionnés à la racine du site ou de la rubrique <i>(articles affichés dans le carrousel)</i>.',
'explication_groupe_mots'=>'Sélectionner le ou les groupes de mots contenant les mots-clé à afficher.',
'explication_image_passe_partout'=>'Rogne le logo pour l\'afficher aux dimensions (largeur,hauteur) saisies ci-dessus.',
'explication_image_zoom_hover'=>'Zoom à 110% au survol de l\'image.',
'explication_lien_page_cible'=>'Par défaut, le lien pointe vers la page "mot". <br>Vous pouvez faire pointer ce lien vers la page courante, l\'identifiant du lien sera alors passé en paramètre.',
'explication_liste_span'=>'Par défaut, la liste affichée est une liste simple (les items sont les uns au dessous des autres).<br/> Vous pouvez choisir d\'afficher la liste sous forme de vignettes en 2, 3 ou 4 colonnes.',
'explication_nb_total'=>'Laisser vide pour "tous"',
'explication_pagination'=>'Pour ne pas afficher de pagination, saisissez un nombre supérieur au nombre total.',
'explication_pagination_sans'=>'Laisser vide pour ne pas afficher de pagination.',
'explication_picto' => 'Pour modifier le pictograme, choisissez en un autre  sur <a href="?exec=picto" title="Fontawesome" class="spip-out">Fontawesome</a>.',
#'explication_rubriques_exclues'=>'Cliquer sur "Ajouter" à droite et sélectionner les rubriques dont les articles sont à exclure de la liste.',
'explication_rubriques_choisies'=>'Cliquer sur "Ajouter" à droite et sélectionner les rubriques dont les articles sont à inclure ou exclure de la liste.',
'explication_rubriques_exclues_sites'=>'Cliquer sur "Ajouter" à droite et sélectionner les rubriques dont les sites sont à exclure de la liste.',
//'explication_picto' => 'Par défaut, le pictogramme est " @picto@ <i class="fa fa-@picto@ "></i>".<br/>Vous pouvez le modifier en choisissant un autre pictogramme sur <a href="https://fontawesome.com/v4.7.0/icons/" title="Fontawesome" class="spip-out">Fontawesome</a>',
'explication_sites_partenaires'=>'Saisissez les identifiants des sites que vous voulez afficher (maximum 6), séparés par une virgule.<br/> <i>1. Les sites doivent avoir été préalablement enregistrés sur le site. <br/>2. Les logos des sites seront affichés dans l\'ordre de la saisie.</i>',
'explication_socialtags'=>'Les réseaux sociaux à afficher sont à sélectionner dans la page de configuration du plugin "<a href="./?exec=configurer_socialtags" title="Configurer Socialtags">Socialtags</a>"',
'explication_titre_mot_video'=>'/!\ Pour afficher une ou plusieurs vidéos, il faut que celles-ci soient insérées dans le chapo d\'un article et que cet article soit associé à un mot clé (par défaut "Vidéo à la une"). /!\ ',
'explication_tri'=>'Vous pouvez choisir de trier les articles par date (du plus récent au plus ancien),<br/>par titre (ordre alphabétique) ou par numéro de titre si vous utilisez la numérotation des articles.',
'explication_uai' => 'Le code UAI (anciennement RNE) est composé de 7 chiffres et 1 lettre. On le retrouve dans l\'adresse mail de l\'étalissement. Exemple 0141234A',
'explication_url_minical' => 'Page sur laquelle arrive le visiteur lorsqu\'il clique sur une date du mini-calendrier. Assurez-vous que cette page contient une noisette listant les mêmes objets que ceux du mini-calendrier.',

'facebook_page'=>'Page facebook',

'hauteur' => 'Hauteur',

'label_afficher_descriptif'=>'Afficher le descriptif',
'label_afficher_filtre'=>'Afficher le filtre',
'label_afficher_liste_simple'=>'Afficher une liste simple',
'label_afficher_logo'=>'Afficher le logo',
'label_afficher_nom_site'=>'Afficher le nom du site',
'label_afficher_parent'=>'Afficher la rubrique parent',
/*'label_afficher_portfolio_documents'=>'Choisir l\'affichage (portfolio d\'images et/ou documents)',
'label_afficher_portfolio_et_documents'=>'Afficher le portfolio et les documents',
'label_afficher_portfolio_seul'=>'Afficher uniquement le portfolio',
'label_afficher_documents_seuls'=>'Afficher uniquement les documents joints',*/
'label_afficher_syndics'=>'Afficher les articles syndiqués',
'label_afficher_tags'=>'Afficher les mots-clé',
'label_afficher_titre'=>'Afficher un titre',
'label_article_choisi'=>'Article choisi',
'label_articles_choisis'=>'Articles choisis',
'label_articles_exclus'=>'Articles exclus',
'label_articles_rubrique'=>'N\'afficher que les articles liès à une rubrique',
'label_articles_mot'=>'N\'afficher que les articles liès à un mot-clé',
'label_choix_coordonnees'=>'Modifier les coordonnées',
'label_cible_vue'=>'Noisette à contôler',
'label_compte_facebook'=>'Nom du compte ou de la page facebook',
'label_compte_twitter'=>'Nom du compte Twitter',
'label_conf_avancee'=>'Configuration avancée',
'label_conf_carrousel'=>'Paramètres du carrousel',
'label_conf_items_liste'=>'Paramètres des items de la liste',
'label_conf_liste'=>'Paramètres de la liste',
'label_conf_titre_noisette'=>'Paramètres du titre de la noisette',
'label_couleur_bg'=>'Couleur d\'arrière-plan',
'label_coupe_texte'=>'Nombre de caractères de l\'introduction',
'label_docs_choisis'=>'Documents choisis',
'label_docs_articles_choisis'=>'Documents de l\'article',
'label_docs_rubriques_choisis'=>'Documents de la rubrique',
'label_est_controleur'=>'Ce bloc est-il contrôleur ?',
'label_est_vue'=>'Cette noisette est vue (contôlée)',
'label_est_vue_selection'=>'Bloc vue (contôlé)',
'label_exclure_article_selection'=>'Exclure les articles sélectionnés',
'label_fieldset_affichage'=>'Options d\'affichage',
'label_fieldset_conf_map'=>'Option de la carte',
'label_fieldset_picto'=>'Pictogramme',
'label_fieldset_span'=>'Colonnage',
'label_filtrer_articles'=>'Filtrer les articles',
'label_filtrer_docs'=>'Filtrer les documents',
'label_filtrer_mots'=>'Filtrer les mots-clé',
'label_groupe_mots'=>'Mots du ou des groupes',
'label_hauteur_bloc'=>'Hauteur du bloc',
'label_hauteur_image_carousel'=>'Hauteur de l\'image',
'label_hauteur_logo'=>'Hauteur du logo',
'label_id_mot'=>'Id du mot-clé',
'label_id_rubrique'=>'Id rubrique',
'label_id_rubrique_parent'=>'Identifiant de la rubrique parent',
'label_image_passe_partout'=>'Afficher le logo plein cadre',
'label_image_zoom_hover'=>'Zoom au survol',
'label_largeur_image_carousel'=>'Largeur de l\'image',
'label_largeur_logo'=>'Largeur du logo',
'label_latitude'=>'Latitude',
'label_lien_page_cible'=>'Page cible du lien',
'label_liste_simple'=>'Afficher une liste simple',
'label_liste_span'=>'Afficher la liste en colonnes',
'label_longitude'=>'Longitude',
'label_masquer_adresse'=>'Masquer l\'adresse',
'label_masquer_auteurs'=>'Masquer les auteurs',
'label_masquer_date'=>'Masquer la date',
'label_masquer_date_modif'=>'Masquer la date de modification',
'label_masquer_descriptif'=>'Masquer le descriptif',
'label_masquer_intro'=>'Masquer l\'introduction',
'label_masquer_logo'=>'Masquer le logo',
'label_masquer_mots'=>'Masquer les mots clé',
'label_masquer_mots_non_lies'=>'Masquer les mots clé non liés à au moins un article',
'label_masquer_nom_site'=>'Masquer le nom du site',
'label_masquer_nombre'=>'Masquer le nombre d\'articles',
'label_masquer_nombre_sites'=>'Masquer le nombre de sites',
'label_masquer_pagination'=>'Masquer la pagination',
'label_masquer_picto'=>'Masquer le pictogramme',
'label_masquer_rubrique_en_cours'=>'Masquer la rubrique en cours',
'label_masquer_titre'=>'Masquer le titre',
'label_masquer_titre_groupes'=>'Masquer le titre des groupes',
'label_pagination_articles'=>'Nombre d\'articles à afficher (pas de la pagination)',
'label_nb_breves'=>'Nombre de brèves à afficher',
'label_nb_evenements'=>'Nombre d\'événements à afficher',
'label_nb_sites'=>'Nombre de sites à afficher',
'label_nb_diapos'=>'Nombre de diapositives',
'label_nb_total'=>'Nombre total d\'articles',
'label_picto'=>'Nom du pictogramme',
'label_rubriques_choisies'=>'Rubriques choisies',
'label_rubriques_exclues'=>'Rubriques exclues',
'label_span'=>'Nombre de colonnes',
'label_sites_partenaires'=>'Identifiants des sites des partenaires',
'label_titre'=>'Titre',
'label_uai' => 'Code UAI de l\'établissement',
'label_url_minical' => 'Page de destination du mini-calendrier',
'label_utiliser_sdn_conteneur'=>'Encadrer la noisette d\'un conteneur',
'label_titre_mot'=>'Titre du mot-clé',
'label_tri'=>'Trier par :',
'label_type_liste'=>'Type d\'affichage de la liste.',
/*GIS*/
'nom_carte_gis'=>'Carte GIS',
'description_carte_gis'=>'Carte GIS',
'label_gis_height'=>'Hauteur de la carte, 400px par defaut',
'label_gis_width'=>'Largeur de la carte, 100% par defaut',
'label_gis_maxzoom'=>'Zoom maxi',
'label_gis_minzoom'=>'Zoom mini',
'label_gis_scale'=>'Afficher l\'échelle de la carte',
'label_gis_zoom'=>'Zoom',
'label_gis_objet'=>'0bjet',
'label_gis_id_objet'=>'Identifiant de l\'objet',
'label_gis_objets'=>'Type d\'objets à afficher (fichier json/gis_xx qui génère la source de données)',
'label_gis_zoom_molette'=>'Désactiver le zoom avec la molette de la souris, actif par defaut',
'label_gis_zoom_fullscreen'=>'Afficher un bouton pour passer la carte en plein écran',
'label_gis_lat'=>'Latitude du centre de la carte',
'label_gis_lon'=>'Longitude du centre de la carte',
'label_gis_cluster'=>'Activer le regroupent des points proches',
'label_gis_si_cluster'=>'Regroupement des points proches',
'label_gis_clustermaxzoom'=>'Regroupe les points jusque à ce zoom, mais pas au delà',


'lines' => 'Lignes',
'liste_articles' => 'Liste d\'articles',
'liste_articles_meme_rubrique' => 'Articles frères',
'liste_articles_selection' => 'Liste d\'articles sélectionnés',
'liste_breves' => 'Liste de brèves ',
'liste_documents' => 'Liste des documents',
'liste_evenements' => 'Liste des événements',
'liste_mots' => 'Liste de mots-clé',
'liste_rubriques_soeurs' => 'Liste des rubriques soeurs',
'liste_simple' => 'Liste simple',
'liste_sites' => 'Liste de sites',
'liste_sous_rubriques' => 'Liste des sous-rubriques',
'liste_syndics' => 'Liste d\'articles syndics',
'liste_videos' => 'Liste de vidéos',

'masquer' => 'Masquer',
'masquer_articles_rubriques_articles_selection' => 'Exclure les articles sélectionnés et/ou les articles des rubriques sélectionnées',
'menu' => 'Menu',
'mots_groupes_selection' => 'Les mots des groupes sélectionnes',
'mots_tous' => 'Tous les mots',

'nom_calendrier_mini' => 'Mini-calendrier des évènements',
'non' => 'Non',
'nuage' => 'Nuage de mots-clé',
'num_titre' => 'Numero de titre',
'nous_situer' => 'Nous situer',

'oui' => 'Oui',
	
'rub_cartouche'=>'Cartouche de la rubrique',
'rub_conteneur'=>'Bloc conteneur pour rubrique',
'rub_documents'=>'Documents de la rubrique',
'rub_texte'=>'Texte de la rubrique',

'partenaires'=>'Partenaires',
'pills'=>'Boutons',


'socialtags'=>'Réseaux sociaux',
'sur_le_web'=>'Sur le web',
'sites_partenaires'=>'Sites des partenaires',
   
'tabs' => 'Onglets',
'titre' => 'Titre',
'tweet' => 'Tweet',
'tweets' => 'Tweets',

'zoom' => 'Zoom'
);


